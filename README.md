# Flow Theme for Standard Notes  

Theme for Standard Notes featuring:  
- Auto switching dark mode,  
- Self collapsing tags and notes columns depends on application width.  

--- 
## Screens  

![Responsive layout](https://bitbucket.org/johnsonjzhou/sn-flow-theme/raw/master/sn-flow-theme-responsive-1.1.0.gif)  
*Navigation panel adjusts based on app width.*

![Dark mode built in](https://bitbucket.org/johnsonjzhou/sn-flow-theme/raw/master/sn-flow-theme-dark-mode-1.1.0.png)  
*Auto switching dark mode using media query*  

--- 
## Install  
````
https://bitbucket.org/johnsonjzhou/sn-flow-theme/raw/master/dist/extension.json
````

--- 
## Local install  
Create a copy of `src/local.json` at `/dist/`, then edit the `url` tag.  

---
## License  
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).