'use strict';
const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// read package.json
const npmConfig = require('./package.json');

/**
 * Handles a json manifest transformation within copy-webpack-plugin 
 * by modifying the version value to a supplied version
 * 
 * @param  {Buffer}  buffer - Nodejs buffer object
 * @param  {string=}  [version=1.0.0] - the version number 
 * @return  {string}
 * 
 * @see  https://github.com/webpack-contrib/copy-webpack-plugin#transformer
 * @see  https://nodejs.org/api/buffer.html
 */
const modifyManifest = (buffer, version = '1.0.0') => {
  let manifest = JSON.parse(buffer.toString());

  manifest.version = version;

  // pretty print to JSON with two spaces
  return JSON.stringify(manifest, null, 2);
};

module.exports = {
  entry: {
    'theme': [ './src/_build.scss' ]
  },
  output: {
    path: path.resolve('./dist/'),
    clean: true
  },
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              url: false,
            },
          },
          {
            loader: 'sass-loader', 
            options: {
              implementation: require('sass')
            }
          }
        ]
      }
    ]
  },
  plugins: [
    // theme.css
    new MiniCssExtractPlugin(),

    // extension.json
    new CopyPlugin({
      patterns: [
        { 
          from: './src/extension.json', 
          to: './extension.json', 
          transform: {
            transformer(content, path) {
              return modifyManifest(content, npmConfig.version);
            },
          }
        }
      ]
    }), 

    // clean up
    new RemoveEmptyScriptsPlugin(),
  ]
};
